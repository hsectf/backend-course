# Backend Workshop 1 (18.09)

## Summary
This workshop covers installing tools, PHP interpreter, bootstrapping a new project, writing migrations, linking models using Eloquent Relations, implementing basic controller logic, writing HTTP requests using PhpStorm HTTP Requests, basic debugging using `dd`.
## Preparation
- Retrieve JetBrains Student license
- Download JetBrains Toolbox
- Download JetBrains PhpStorm
- Install PHP >= 7.2
- Install composer
- Create a new Laravel project
- Create a test mysql database (RemoteMysql.com / ...), copy credentials to .env

## Code
- Create models `Post` and `Comments` with `php artisan make:model`
- Write migrations
- `php artisan migrate`
- Implement routing in `api.php` using `apiResources`
- Fill `$fillable` array in models
- Start server with `php artisan serve`
- Write requests
- Implement Controllers' logic

## Recomendations
- Install Insomnia for making requests
- Install Git
- Install Mingw Bash & ConEmu (Windows users)
- Install Brew (MacOS users)
- Watch [Laracasts](https://laracasts.com) lessons
